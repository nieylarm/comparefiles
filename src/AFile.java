import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AFile {
    public static final String B = "B";
    public static final String KB = "KB";
    public static final String MB = "MB";

    private final File file;

    public AFile(File file) {
        this.file = file;
    }

    public String getName() {
        return file != null ? file.getName() : "<no name>";
    }

    public String getExt() {
        if (file != null) {
            String name = file.getName();
            if (name != null && !name.trim().isEmpty()) {
                int index = name.lastIndexOf('.');
                if (index > 0) {
                    String ext = name.substring(index);
                    return ext;
                }
            }
        }
        return "";
    }

    public long getLength() {
        return file != null ? file.length() : 0;
    }

    public double getSize() {
        return getSize(MB);
    }

    public double getSize(String unit) {
        double bytes = getLength();
        double kilobytes = (bytes / 1024);
        double megabytes = (kilobytes / 1024);
        /*
         * double gigabytes = (megabytes / 1024); double terabytes = (gigabytes / 1024); double petabytes = (terabytes /
         * 1024); double exabytes = (petabytes / 1024); double zettabytes = (exabytes / 1024); double yottabytes =
         * (zettabytes / 1024);
         */
        if (B.equals(unit))
            return bytes;
        if (KB.equals(unit))
            return kilobytes;
        if (MB.equals(unit))
            return megabytes;
        return bytes;
    }

    public File getFile() {
        return file;
    }

    public List<AFile> getFileOnlyList() {
        List<AFile> files = new ArrayList<AFile>(1);
        files.add(this);
        return files;
    }

    public List<AFile> getFileOnlyList(String ext) {
        if (ext != null && !ext.trim().isEmpty()) {
            List<AFile> files = new ArrayList<AFile>(1);
            files.add(this);
            return files;
        }
        return getFileOnlyList();
    }

    public boolean hasSameName(AFile aFile) {
        return (aFile != null && aFile.getName().equals(getName()));
    }

    public boolean hasSameSize(AFile aFile) {
        return (aFile != null && aFile.getLength() == getLength());
    }

    public boolean equals(AFile aFile) {
        return hasSameName(aFile) && hasSameSize(aFile);
    }

    public String toString() {
        return this.getName();
    }

}
