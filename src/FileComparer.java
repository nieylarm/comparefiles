
import java.util.ArrayList;
import java.util.List;

public class FileComparer {

    private final AFile file1;
    private final AFile file2;
    private final String ext;
    
    public FileComparer(String p1, String p2) {
        FileExplorer explorer = new FileExplorer(p1);
        file1 = explorer.getaFile();
        explorer = new FileExplorer(p2);
        file2 = explorer.getaFile();
        this.ext = "";
    }

    public FileComparer(String p1, String p2, String ext) {
        FileExplorer explorer = new FileExplorer(p1);
        file1 = explorer.getaFile();
        explorer = new FileExplorer(p2);
        file2 = explorer.getaFile();
        this.ext = ext;
    }

    public void compare() {
        List<AFile> list1 = file1.getFileOnlyList(ext);
        List<AFile> list2 = file2.getFileOnlyList(ext);
        List<AFile> commons1 = new ArrayList<AFile>();
        List<AFile> commons2 = new ArrayList<AFile>();
        List<AFile> diff1 = new ArrayList<AFile>();
        List<AFile> diff2 = new ArrayList<AFile>();

        for (int i = 0; i < list1.size(); i++) {
            AFile e1 = list1.get(i);
            for (int j = 0; j < list2.size(); j++) {
                AFile e2 = list2.get(j);
                if (e2.equals(e1)) {
                //if (e2.hasSameName(e1)) {
                    commons1.add(e1);
                    commons2.add(e2);
                    break;
                }
            }
        }

        // copy all lists to diff lists
        diff1.addAll(list1);
        diff2.addAll(list2);

        // remove common file from original lists
        for (AFile e : commons1) {
            diff1.remove(e);
        }
        for (AFile e : commons2) {
            diff2.remove(e);
        }

        // reporting
        System.out.println("Comparing " + file1.getFile().getPath() + " and " + file2.getFile().getPath());
        System.out.printf("Total files 1: %d \n", list1.size());
        System.out.printf("Total files 2: %d \n", list2.size());
        System.out.printf("Common files 1: %d \n", commons1.size());
        System.out.printf("Common files 2: %d \n", commons2.size());
        System.out.printf("Diff in first list: %d \n", diff1.size());
        System.out.printf("Diff in second list: %d \n", diff2.size());

        if (!diff1.isEmpty()) {
            System.out.println("\nDiff files in " + file1.getFile().getPath());
            for (AFile aFile : diff1) {
                System.out.println(aFile);
            }
        }
        if (!diff2.isEmpty()) {
            System.out.println("\nDiff files in " + file2.getFile().getPath());
            for (AFile aFile : diff2) {
                System.out.println(aFile);
            }
        }
        System.out.println("===========================================");
    }

    public static void main(String[] args) {
        String p1 = "C:\\data";
        String p2 = "C:\\data";
        p1 = "C:\\Users\\608509165\\Documents\\BUT\\JarFileComparison\\trunk\\newBTWS\\newBTWS\\WEB-INF\\lib";
        p2 = "C:\\Users\\608509165\\Documents\\BUT\\JarFileComparison\\trunk\\newBTWS_LIVE\\newBTWS\\WEB-INF\\lib";

        FileComparer comparer = new FileComparer(p1, p2);

        comparer.compare();
    }
}
