import java.util.Date;

public class FileInfo {
    private String permission;
    private int linkCount;
    private String owner;
    private String group;
    private long length;
    private Date modified;
    private String path;
    private String name;

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public int getLinkCount() {
        return linkCount;
    }

    public void setLinkCount(int linkCount) {
        this.linkCount = linkCount;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExt() {
        if (name != null && !name.trim().isEmpty()) {
            int index = name.lastIndexOf('.');
            if (index > 0) {
                String ext = name.substring(index);
                return ext;
            }
        }
        return "";
    }

    public boolean hasSameName(FileInfo fileInfo) {
        return (fileInfo != null && fileInfo.getName() != null && fileInfo.getName().equals(getName()));
    }

    public boolean hasSamePath(FileInfo fileInfo) {
        return (fileInfo != null && fileInfo.getPath() != null && fileInfo.getPath().equals(getPath()));
    }

    public boolean hasSameSize(FileInfo fileInfo) {
        return (fileInfo != null && fileInfo.getLength() == getLength());
    }

    public boolean equals(FileInfo fileInfo) {
        return hasSameName(fileInfo) && hasSameSize(fileInfo) && hasSamePath(fileInfo);
    }

    public String toString() {
        return this.getName();
    }

}
