import java.util.ArrayList;
import java.util.List;

public class FileInfoComparer {

    private final FileInfoReader reader1;
    private final FileInfoReader reader2;
    private final String ext;

    public FileInfoComparer(String p1, String p2) {
        reader1 = new FileInfoReader();
        reader1.read(p1);

        reader2 = new FileInfoReader();
        reader2.read(p2);

        this.ext = "";
    }

    public FileInfoComparer(String p1, String p2, String ext) {
        reader1 = new FileInfoReader();
        reader1.read(p1);

        reader2 = new FileInfoReader();
        reader2.read(p2);

        this.ext = ext;
    }

    public void compare() {
        List<FileInfo> list1 = reader1.getFileInfoList().filter(ext);
        List<FileInfo> list2 = reader2.getFileInfoList().filter(ext);

        List<FileInfo> commons1 = new ArrayList<FileInfo>();
        List<FileInfo> commons2 = new ArrayList<FileInfo>();

        List<FileInfo> diff1 = new ArrayList<FileInfo>();
        List<FileInfo> diff2 = new ArrayList<FileInfo>();

        for (int i = 0; i < list1.size(); i++) {
            FileInfo e1 = list1.get(i);
            for (int j = 0; j < list2.size(); j++) {
                FileInfo e2 = list2.get(j);
                if (e2.equals(e1)) {
                    // if (e2.hasSameName(e1)) {
                    commons1.add(e1);
                    commons2.add(e2);
                    break;
                }
            }
        }

        // copy all lists to diff lists
        diff1.addAll(list1);
        diff2.addAll(list2);

        // remove common file from original lists
        for (FileInfo e : commons1) {
            diff1.remove(e);
        }
        for (FileInfo e : commons2) {
            diff2.remove(e);
        }

        // reporting
        System.out.println("Comparing " + reader1.getPath() + " and " + reader2.getPath());
        System.out.printf("Total files 1: %d \n", list1.size());
        System.out.printf("Total files 2: %d \n", list2.size());
        System.out.printf("Common files 1: %d \n", commons1.size());
        System.out.printf("Common files 2: %d \n", commons2.size());
        System.out.printf("Diff in first list: %d \n", diff1.size());
        System.out.printf("Diff in second list: %d \n", diff2.size());

        if (!diff1.isEmpty()) {
            System.out.println("\nDiff files in " + reader1.getPath());
            for (FileInfo aFile : diff1) {
                System.out.println(aFile);
            }
        }
        if (!diff2.isEmpty()) {
            System.out.println("\nDiff files in " + reader2.getPath());
            for (FileInfo aFile : diff2) {
                System.out.println(aFile);
            }
        }
        System.out.println("===========================================");
    }

    public static void main(String[] args) {
        String p1 = "JAR.txt";
        String p2 = "dev.txt";

        FileInfoComparer comparer = new FileInfoComparer(p1, p2, ".jar");

        comparer.compare();
    }
}
