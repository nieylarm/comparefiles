import java.util.ArrayList;
import java.util.List;

public class FileInfoList {
    private List<FileInfo> fileInfoList;

    public List<FileInfo> getFileInfoList() {
        return fileInfoList;
    }

    public void setFileInfoList(List<FileInfo> fileInfoList) {
        this.fileInfoList = fileInfoList;
    }

    public boolean add(FileInfo fileInfo) {
        if (fileInfoList == null) {
            fileInfoList = new ArrayList<FileInfo>();
        }
        return fileInfoList.add(fileInfo);
    }

    public List<FileInfo> filter(String ext) {
        if (ext != null && !ext.trim().isEmpty()) {
            if (fileInfoList != null) {
                List<FileInfo> list = new ArrayList<FileInfo>(fileInfoList.size());
                for (FileInfo fileInfo : fileInfoList) {
                    if (fileInfo != null) {
                        String fileExt = fileInfo.getExt();
                        if (fileExt != null && fileExt.equals(ext)) {
                            list.add(fileInfo);
                        }
                    }
                }
                return list;
            }
        }
        return fileInfoList;
    }

}
