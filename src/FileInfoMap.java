import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FileInfoMap {
    public static final String PERMISSION = "permission";
    public static final String LINK_COUNT = "linkCount";
    public static final String OWNER = "owner";
    public static final String GROUP = "group";
    public static final String LENGTH = "length";
    public static final String MONTH = "month";
    public static final String DATE = "date";
    public static final String TIME = "time";
    public static final String PATH = "path";

    public static final String[] KEYS = { PERMISSION, LINK_COUNT, OWNER, GROUP, LENGTH, MONTH, DATE, TIME, PATH };

    private Map<String, String> infoMap;

    public Map<String, String> getInfoMap() {
        return infoMap;
    }

    public void setInfoMap(Map<String, String> infoMap) {
        this.infoMap = infoMap;
    }

    public String put(String key, String value) {
        if (infoMap == null) {
            infoMap = new HashMap<String, String>();
        }
        return infoMap.put(key, value);
    }

    public String get(String key) {
        if (infoMap != null) {
            return infoMap.get(key);
        }
        return null;
    }

    protected int parseInt(String n) {
        if (n != null) {
            try {
                return Integer.parseInt(n);
            } catch (Exception e) {
            }
        }
        return 0;
    }

    protected long parseLong(String n) {
        if (n != null) {
            try {
                return Long.parseLong(n);
            } catch (Exception e) {
            }
        }
        return 0;
    }

    protected Date parseDate(String month, String date, String time) {
        if (month != null && !month.trim().isEmpty() && date != null && !date.trim().isEmpty() && time != null
                && !time.trim().isEmpty()) {
            String source = String.format("%s %s %s", month, date, time);
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d HH:mm");
            try {
                return dateFormat.parse(source);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    protected String getNameFromPath(String path) {
        if (path != null && !path.trim().isEmpty()) {
            int index = path.lastIndexOf('/');
            if (index >= 0) {
                return path.substring(index + 1);
            }
        }
        return path;
    }

    protected FileInfo toInfo() {
        if (infoMap != null) {
            FileInfo fileInfo = new FileInfo();

            String permission = infoMap.get(PERMISSION);
            String linkCount = infoMap.get(LINK_COUNT);
            String owner = infoMap.get(OWNER);
            String group = infoMap.get(GROUP);
            String length = infoMap.get(LENGTH);
            String month = infoMap.get(MONTH);
            String date = infoMap.get(DATE);
            String time = infoMap.get(TIME);
            String path = infoMap.get(PATH);

            fileInfo.setPermission(permission);
            fileInfo.setLinkCount(parseInt(linkCount));
            fileInfo.setOwner(owner);
            fileInfo.setGroup(group);
            fileInfo.setLength(parseLong(length));
            fileInfo.setModified(parseDate(month, date, time));
            fileInfo.setPath(path);
            fileInfo.setName(getNameFromPath(path));

            return fileInfo;
        }
        return null;
    }

    public String toString() {
        return infoMap.toString();
    }
}
