import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class FileInfoReader {
    private String path;
    private FileInfoList fileInfoList;

    protected FileInfoMap toMap(String line) {
        if (line != null && !line.trim().isEmpty()) {
            FileInfoMap fileInfoMap = new FileInfoMap();
            Scanner scanner = null;
            try {
                scanner = new Scanner(line);
                int index = 0;
                while (scanner.hasNext()) {
                    String token = scanner.next();
                    if (token != null && !token.trim().isEmpty()) {
                        fileInfoMap.put(FileInfoMap.KEYS[index], token);
                        index++;
                    }
                }
                return fileInfoMap;
            } finally {
                if (scanner != null) {
                    scanner.close();
                }
            }
        }
        return null;
    }

    public void read(String path) {
        if (path != null && !path.trim().isEmpty()) {
            this.path = path;
            fileInfoList = new FileInfoList();
            Scanner scanner = null;
            try {
                scanner = new Scanner(new File(path));

                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();

                    if (line != null && !line.trim().isEmpty()) {
                        FileInfoMap fileInfoMap = toMap(line);
                        if (fileInfoMap != null) {
                            FileInfo fileInfo = fileInfoMap.toInfo();
                            if (fileInfo != null) {
                                fileInfoList.add(fileInfo);
                            }
                        }
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public FileInfoList getFileInfoList() {
        return fileInfoList;
    }

    public void setFileInfoList(FileInfoList fileInfoList) {
        this.fileInfoList = fileInfoList;
    }

    public static void main(String[] args) {
        String path = "JAR.txt";
        FileInfoReader reader = new FileInfoReader();
        reader.read(path);

        FileInfoList fileInfoList = reader.getFileInfoList();
        if (fileInfoList != null) {
            List<FileInfo> list = fileInfoList.getFileInfoList();
            if (list != null) {
                for (FileInfo fileInfo : list) {
                    System.out.println(fileInfo.getName());
                }
            }
        }
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
