import java.util.List;

public class FileListGenerator {
    public static void main(String[] args) {
        String dir = "C:\\Users\\608509165\\Documents\\BUT\\JarFileComparison\\trunk\\newBTWS\\newBTWS\\WEB-INF\\lib";
        FileExplorer explorer = new FileExplorer(dir);
        AFile aFile = explorer.getaFile();
        List<AFile> aFiles = aFile.getFileOnlyList();
        
        String format = "%s \t\t %d";
        if (aFiles != null && !aFiles.isEmpty()) {
            for (AFile file : aFiles) {
                String name  = file.getName();
                long length = file.getLength();
                String line = String.format(format, name, length);
                System.out.println(line);
            }
        }
        
    }
}
