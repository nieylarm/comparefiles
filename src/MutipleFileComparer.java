import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MutipleFileComparer {
    public static void main(String[] args) {
        String filename = "paths2.txt";
        File file = new File(filename);
        if (file.exists() && file.canRead()) {
            Scanner scanner = null;
            try {
                scanner = new Scanner(file);
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    if (line != null && !line.trim().isEmpty()) {
                        String[] splits = line.split(",");
                        if (splits != null && splits.length >= 2) {
                            String p1 = splits[0];
                            String p2 = splits[1];

                            FileComparer comparer = new FileComparer(p1, p2, ".jar");

                            comparer.compare();
                        }
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (scanner != null) {
                    scanner.close();
                }
            }
        }
    }
}
